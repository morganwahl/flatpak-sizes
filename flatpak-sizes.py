#!/usr/bin/env python3
import sys
import re
import sh
from decimal import Decimal as D
import configparser


def _flatpack_list():
	return [
		dict(zip(
			('ref', 'origin', 'active_commit', 'latest_commit', 'size_printed', 'size_unit', 'options'),
			l.split(),
		))
		for l in (
			sh.flatpak
			.list(show_details=True, _tty_out=False)
			.splitlines()
		)
	]


def _flatpak_metadata(ref):
	data_string = str(sh.flatpak.info(ref, show_metadata=True))
	data = configparser.ConfigParser()
	data.read_string(data_string)
	metadata = {}
	for section in data.sections():
		metadata[section] = dict(data[section].items())
	return metadata


def get_package_data():
	package_list = _flatpack_list()
	for entry in package_list:
		entry['size'] = D(entry['size_printed']) * {
			'kB': 10 ** 3,
			'MB': 10 ** 6,
		}[entry['size_unit']]
		
		name, arch, channel = entry['ref'].split('/')
		entry['name'] = name
		entry['arch'] = arch
		entry['channel'] = channel
		entry['icon'], entry['shortname'] = name.rsplit('.', maxsplit=1)

	for package in package_list:
		metadata = _flatpak_metadata(package['ref'])
		package['metadata'] = metadata

		package['runtimes'] = []
		if 'Runtime' in metadata:
			package['runtimes'].append(metadata['Runtime']['runtime'])
		if 'Application' in metadata:
			package['runtimes'].append(metadata['Application']['runtime'])

	return package_list

def ref_to_identifier(ref):
	identifier = ''
	for character in ref:
		if re.match(r'[a-zA-Z0-9]', character):
			identifier += character
		else:
			identifier += '_' + str(ord(character))
	return identifier

def make_dot(package_data):
	dot = []
	dot.append("digraph flatpak {")
	for package in package_data:
		size = package['size'] ** D('.5')
		size = size / 100
		size = size / 96
		dot.append("{} [label=\"{}\", height=\"{}\", width=\"{}\", fixedsize=true];".format(
			ref_to_identifier(package['ref']),
			package['shortname'],
			size,
			size,
		))
	for package in package_data:
		for runtime in package['runtimes']:
			dot.append("{} -> {} [label=\"{}\"];".format(
				ref_to_identifier(package['ref']),
				ref_to_identifier(runtime),
				'runtime',
			))
	dot.append("}")
	return '\n'.join(dot) + "\n"


def main():
	packages = get_package_data()
	dot_data = make_dot(packages)
	with open('output.dot', 'w') as out:
		out.write(dot_data)
		

if __name__ == '__main__':
	main()
